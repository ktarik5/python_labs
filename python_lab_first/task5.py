# coding=utf-8
"""1.	Create function that takes list of strings as input argument.
2.	Each string is a number – “1”, “5”, “35”, “456”…
3.	Transform the list of strings to list of numbers
4.	Sort the transformed list in ascending order and return it.

"""


def transformset(strlist):
    list_numbers = []
    if type(strlist) is list:
        for i in strlist:
            list_numbers.append(int(i))
        list_numbers = sorting(list_numbers)
        return list_numbers
    else:
        return "method's argument should be list"


def sorting(list_numbers):
    for i in range(len(list_numbers) - 1):
        for j in range(i, len(list_numbers)):
            if list_numbers[i] > list_numbers[j]:
                temp = list_numbers[j]
                list_numbers[j] = list_numbers[i]
                list_numbers[i] = temp
    return list_numbers
