# coding=utf-8
"""1.	Create function that takes “X” and “Y” as input parameters.
2.	Function returns list of random numbers in a range(0, Y) with X size
"""

import random as r


def myrandom(x, y):
    random_list = []
    for i in range(0, x):
        random_list.append(r.randint(0, y))
    return random_list
