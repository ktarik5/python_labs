# coding=utf-8
"""1.	Create function that takes Dictionary as input argument.
2.	The “key” of dictionary is “username” and “value” is “password”.
3.	Access validation algorithm should be implemented this way: username > password. For example:
a.	[“user”, “sigma”] – access denied, as the size(“user”) = 4 < size(“sigma ”) = 5
b.	[“Administrator”,” sigma”] – Access granted, as the size (“Administrator”) = 13 > size (“sigma”) = 5
4.	Dictionary should contain 5 or more key-value pairs. (1 pair = 1 user)
5.	Function should return “true” if at least one of the users has access. Other ways, should return “false”.
6.	If “valid” user found interrupt iterating (use “break” operator)
"""


def useracces(dict):
    count = False
    if (len(dict) >= 5):
        for k, v in dict.items():
            if len(k) > len(v):
                count = True
                break
        return count
    else:
        return "dictionary has less than 5 pairs"

