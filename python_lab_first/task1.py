# coding=utf-8
""" Implement function that returns bigger value out of two expressions:
a.	sin (x) / cos (x)
b.	cos (x) / sin (x)
"""

import math as m


def compare_cos_sin(x):
    a = m.cos(x) / m.sin(x)
    b = m.sin(x) / m.cos(x)
    if a > b:
        return a
    elif b > a:
        return b
    else:
        return "they are equal"
