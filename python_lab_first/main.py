import python_lab_first.task1
import python_lab_first.task2
import python_lab_first.task3
import python_lab_first.task4
import python_lab_first.task5
import python_lab_first.task6
import python_lab_first.task7
import python_lab_first.task8
import python_lab_first.additional_task

print("task 1: ", python_lab_first.task1.compare_cos_sin(15))  # task 1

dict = python_lab_first.task2.fibo_nunmbers(10)  # task 2
print("task 2:")
for k, v in dict.items():
    print(k, v)

str = ['asdas', 'a', 'asdasdasd', 'qqqqqq', '']
strnew = ('asdas', 'a', 'asdasdasd', 'qqqqqq', '')
print("task 3: ", python_lab_first.task3.list_qty(str))  # task 3

print("task 4: ", python_lab_first.task4.list_with_aliquot_to_3(str))  # task 4

liststr = ["1", "345", "5", "555", "45", "343", "7"]
print("task 5: ", python_lab_first.task5.transformset(liststr))  # task 5

print("task 6: ", python_lab_first.task6.stringreplace("test for test", "test", "temp"))  # task 6

dict = {"sigma": "password", "administrator": "test", "testuser": "testuser", "temp": "t", "sa": "user"}
print("task 7: ", python_lab_first.task7.useracces(dict))  # task 7

print("task 8:", python_lab_first.task8.myrandom(3, 10))  # task 8

int_list = [1, 2, 3, 5, 6, 7, 9, 10]
print("additional task: ", python_lab_first.additional_task.binary_search(int_list, 10))  # additional task
