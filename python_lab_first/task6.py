# coding=utf-8
""" Create function ‘replace’, that takes three arguments: string (where you search a text), old text (that should be removed), new text (that should be added in the string)"""


def stringreplace(stringoriginal, old, new):
    ind = str_find(old, stringoriginal)
    ind2 = ind + len(old)
    while ind >= 0:
        if ind == 0:
            stringoriginal = new + stringoriginal[ind2:len(stringoriginal)]
        else:
            stringoriginal = stringoriginal[0:ind] + new + stringoriginal[ind2:len(stringoriginal)]
        ind = str_find(old, stringoriginal)
        ind2 = ind + len(old)
    return stringoriginal


def str_find(str_to_find, str_where_to_find):
    str_len = len(str_to_find)
    for i in range(0, len(str_where_to_find)):
        if (str_where_to_find[i:i + str_len] == str_to_find):
            return i
    return -1
