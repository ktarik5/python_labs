# coding=utf-8
"""Create function that returns dictionary of Fibonacci numbers in the following format:
Key = index of found element: 0, 1, 2, 3, 4, 5…
Value = Fibonacci number
If the number is even, place ‘{#} is even’ text insetad of the number.
Example, how it should like:
{
‘0’: ‘0’,
‘1’: ‘1’,
‘2’: ‘1’,
‘3’: ‘2 is even.
‘4’: ‘3’,
‘5’: ‘5’,
‘6’: ‘8 is even’
…}
"""


def fibo_nunmbers(n):
    dict = {0: 0}
    a = 0
    b = 1
    for i in range(1, n):
        a, b = b, a + b
        if b % 2 == 0:
            dict[i] = str(b) + " is even"
        else:
            dict[i] = b
    return dict
