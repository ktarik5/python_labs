# coding=utf-8
"""Create function that takes List of strings as input argument and returns the quantity of elements with size aliquot to three. (Element’s size is the length of the string – number of symbols)"""


def list_qty(strlist):
    count = 0
    if type(strlist) is list:
        for item in strlist:
            if item != "" and len(item) % 3 == 0:
                count += 1
        return count
    else:
        return "method's arguments should be list"
