# coding=utf-8
"""1.	Create function that takes List of strings as input argument
2.	Removes the elements which size is aliquot to 3 from the input list
3.	Returns updated list
"""


def list_with_aliquot_to_3(strlist):
    for s in strlist[:]:
        if s != "" and len(s) % 3:
            strlist.remove(s)
    return strlist
