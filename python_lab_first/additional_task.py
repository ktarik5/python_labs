# coding=utf-8
""" Implement function that returns indext in the sorted list using binary search
"""


def binary_search(int_list, number_to_find):
    if (len(int_list) == 1):
        return 0
    if (len(int_list) % 2 == 0):
        sub_list_len = int(len(int_list) / 2)
    else:
        sub_list_len = int((len(int_list) - 1) / 2)
    left = int_list[0:sub_list_len]
    right = int_list[sub_list_len:len(int_list)]
    if left[-1] >= number_to_find:
        return binary_search(left, number_to_find)

    else:

        return binary_search(right, number_to_find) + len(left)
