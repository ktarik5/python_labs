import math as m
from abc import ABCMeta, abstractmethod


class Shape:
    __metaclass__ = ABCMeta

    def __add__(self, other):
        return self.area() + other.area()

    def __eq__(self, other):
        if self.area() == other.area():
            return "equal"
        else:
            return "not equal"

    @abstractmethod
    def area(self):
        pass


class Square(Shape):

    def __init__(self, side):
        self.side = side

    def area(self):
        return self.side ** 2

    def __str__(self):
        return "Square with side {} and area {}".format(self.side, self.area())


class Circle(Shape):

    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return m.pi * self.radius ** 2

    def __str__(self):
        return "Circle with radius {} and area {}".format(self.radius, self.area())


class Triangle(Shape):

    def __init__(self, side_a, side_b, side_c):
        self.side_a = side_a
        self.side_b = side_b
        self.side_c = side_c

    def area(self):
        p = (self.side_a + self.side_b + self.side_c) / 2
        return m.sqrt(p * (p - self.side_a) * (p - self.side_b) * (p - self.side_c))

    def __str__(self):
        return "Triangele with sides {}, {}, {} and area {}".format(self.side_a, self.side_b, self.side_c, self.area())
