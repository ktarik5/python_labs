import python_lab_second.task4.shapes as s

# task4
print("*****************TASK4***************")
circle = s.Circle(5)
triangle = s.Triangle(3, 4, 5)
square = s.Square(10)

print('CLASS ARGUMENTS:')
print(circle.__str__())
print(square.__str__())
print(triangle.__str__())

print('\nMETHOD OVERLOADING:')
print("Circle is", circle == square, "to square")
print(circle + square)
