from python_lab_second.task2.person import Person
from abc import ABCMeta, abstractmethod


class Teacher(Person):
    __metaclass__ = ABCMeta

    def __init__(self, first_name, last_name, age, base_wage, total_hours_worked, department):
        super(Teacher, self).__init__(first_name, last_name, age)
        self.base_wage = base_wage
        self.department = department
        self.total_hours_worked = total_hours_worked

    def get_percent_by_department(self):
        if self.department == "programming":
            return 50
        elif self.department == "arts":
            return 60
        elif self.department == "languages":
            return 70
        else:
            return 0

    def count_salary(self):
        return self.base_wage + (
                self.base_wage / 160 * self.get_percent_by_department() / 100 * self.total_hours_worked)


class TeacherProgrammingDepartment(Teacher):
    def __init__(self, first_name, last_name, age, base_wage=3200, total_hours_worked=160):
        super(TeacherProgrammingDepartment, self).__init__(first_name, last_name, age, base_wage, total_hours_worked,
                                                           "programming")

    def __str__(self):
        return "Teacher {} {}, {}: earns {}".format(self.get_first_name(), self.get_last_name(), self.get_age(),
                                                    self.count_salary())


class TeacherArtsDepartment(Teacher):
    def __init__(self, first_name, last_name, age, base_wage=3200, total_hours_worked=160):
        super(TeacherArtsDepartment, self).__init__(first_name, last_name, age, base_wage, total_hours_worked, "arts")

    def __str__(self):
        return "Teacher {} {}, {}: earns {}".format(self.get_first_name(), self.get_last_name(), self.get_age(),
                                                    self.count_salary())


class TeacherLanguagesDepartment(Teacher, object):
    def __init__(self, first_name, last_name, age, base_wage=3200, total_hours_worked=160):
        super(TeacherLanguagesDepartment, self).__init__(first_name, last_name, age, base_wage, total_hours_worked,
                                                         "languages")

    def __str__(self):
        return "Teacher {} {}, {}: earns {}".format(self.get_first_name(), self.get_last_name(), self.get_age(),
                                                    self.count_salary())


class Student(Person):

    def __init__(self, first_name, last_name, age, courses):
        super(Student, self).__init__(first_name, last_name, age)
        self.courses = courses

    def get_course(self):
        return self.courses

    def __str__(self):
        return "Student {} {}, {}: learns {}".format(self.get_first_name(), self.get_last_name(), self.get_age(),
                                                     self.get_course())
