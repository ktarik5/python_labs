from python_lab_second.task2.university import Teacher, Student, TeacherLanguagesDepartment, TeacherArtsDepartment, \
    TeacherProgrammingDepartment

# task2
print("*****************TASK2***************")
first_student = Student("Dmytro", "Petrenko", 30, "math")
second_student = Student("Valentyn", "Sydorenko", 25, "programming")
third_student = Student("Igor", "Pups", 18, "arts")

print('STUDENTS INFO:')
print(first_student.__str__())
print(second_student.__str__())
print(third_student.__str__())

first_teacher = TeacherLanguagesDepartment("Kyrylo", "Kogut", 50, 3200)
second_teacher = TeacherProgrammingDepartment("Valentyna", "Barchyshyn", 37, 3700, 170)
third_teacher = TeacherArtsDepartment("Lesya", "Matviiv", 44, 4500, 200)

print('\nTEACHERS EARN:')
print(first_teacher.__str__())
print(second_teacher.__str__())
print(third_teacher.__str__())
