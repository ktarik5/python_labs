class Bank:
    customer = ""
    balance = 0
    interest_rate = 0
    number_of_months = 0


class Deposit(Bank):
    def depositing(self, deposit_sum):
        return self.balance + deposit_sum

    def withdrawing(self, withdrawing_sum):
        return self.balance - withdrawing_sum

    def calculate_interest(self):
        if self.balance < 0 or self.balance > 1000:
            return self.number_of_months * self.interest_rate
        else:
            self.interest_rate = 0
            return self.interest_rate


class Loan(Bank):
    def depositing(self, deposit_sum):
        return self.balance + deposit_sum

    def calculate_interest(self):
        if self.number_of_months > 3 and self.customer == "individual":
            return self.number_of_months * self.interest_rate
        elif self.number_of_months > 2 and self.customer == "company":
            return self.number_of_months * self.interest_rate
        else:
            self.interest_rate = 0
            return self.interest_rate


class Mortgage(Bank):
    def depositing(self, deposit_sum):
        return self.balance + deposit_sum

    def calculate_interest(self):
        if self.balance < 0 or self.balance > 1000:
            return self.number_of_months * self.interest_rate
        else:
            self.interest_rate = 0
            return self.interest_rate
