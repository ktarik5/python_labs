from python_lab_second.task1.child_classes import Dog, Cat, Mouse, Frog
from python_lab_second.task2.university import Teacher, Student
import python_lab_second.task3.products as p
import python_lab_second.task4.shapes as s

# task1
print("*****************TASK1***************")
dog = Dog()
dog.name = "Brovko"
dog.age = 2
dog.gender = "M"
cat = Cat()
cat.name = "Murka"
cat.age = 5
cat.gender = "W"
mouse = Mouse()
mouse.name = "Pyptyk"
mouse.age = 1
mouse.gender = "M"
frog = Frog()
frog.name = "Prince"
frog.age = 10
frog.name = "M"

print("Dog ", dog.name, ", Age ", dog.age, ", Gender ", dog.gender)
print("Cat ", cat.name, ", Age ", cat.age, ", Gender ", cat.gender)
print("Mouse ", mouse.name, ", Age ", mouse.age, ", Gender ", mouse.gender)
print("Frog ", frog.name, ", Age ", frog.age, ", Gender ", frog.gender)
dog.make_sound("gav")
cat.make_sound("miav")
mouse.make_sound("pi")
frog.make_sound("rabit")
print("Dog eats ", dog.eat("meat", "pepsi"))
print("Cat eats ", cat.eat("Whiskas"))
print("Mouse eats ", mouse.eat("cheese"))
print("Frog eats ", frog.eat("meatbugsmeat", "supebugsmeat"))

# task2
print("*****************TASK2***************")
first_student = Student("Dmytro", "Petrenko", 30, "math")
second_student = Student("Valentyn", "Sydorenko", 25, "programming")
third_student = Student("Igor", "Pups", 18, "arts")
print(first_student.first_name, first_student.last_name, "learns", first_student.courses)
print(second_student.first_name, second_student.last_name, "learns", second_student.courses)
print(third_student.first_name, third_student.last_name, "learns", third_student.courses)

first_teacher = Teacher("Kyrylo", "Kogut", 50, "languages", 100)
first_teacher.total_hours_worked = 160
second_teacher = Teacher("Valentyna", "Barchyshyn", 37, "programming", 150)
second_teacher.total_hours_worked = 170
third_teacher = Teacher("Lesya", "Matviiv", 44, "arts", 200)
third_teacher.total_hours_worked = 200

print(first_teacher.first_name, first_teacher.last_name, "earns", first_teacher.teacher_salary())
print(second_teacher.first_name, second_teacher.last_name, "earns", second_teacher.teacher_salary())
print(third_teacher.first_name, third_teacher.last_name, "earns", third_teacher.teacher_salary())

# task3
print("*****************TASK3***************")

main = p.Product()
print(main.__str__())

iphone = p.Electronics(500, 1325, "black", "Apple", 10, "iOS", "1900*1200", True)
print(iphone.__str__())

nike = p.Sneakers(1000, 5423, "red", "Nike", "nylon", 42, "rubber", True)
print(nike.__str__())

dumbbells = p.Fitness_equipment()
print(dumbbells.__str__())

# task4
print("*****************TASK4***************")
circle = s.Circle(5)
triangle = s.Triangle(3,4,5)
square = s.Square(10)
print(circle.__str__())
print(square.__str__())
print(triangle.__str__())
print("Circle area", circle.area())
print("Square area", square.area())
print("Triangle area", triangle.area())

print("Circle is", circle==square, "to square")
print(circle+square)