import python_lab_second.task3.electronics as El
import python_lab_second.task3.sports as Sp
import python_lab_second.task3.clothes as Cl

# task3
print("*****************TASK3***************")

iphone = El.SmartPhones(500, 1325, "black", "Apple", 10, "iOS", "1900*1200", True)
print(iphone.__str__())

nike = Cl.Sneakers(1000, 5423, "red", "Nike", "nylon", 42, "rubber", True)
print(nike.__str__())

dumbbells = Sp.FitnessEquipment()
print(dumbbells.__str__())
