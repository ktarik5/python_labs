from python_lab_second.task3.products import Product


class Clothes(Product, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', material="textile", size=44):
        super(Clothes, self).__init__(price, product_code, color, brand)
        self.material = material
        self.size = size


class Sneakers(Clothes, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', material="textile", size=44,
                 sole="rubber", lacing=False):
        super(Sneakers, self).__init__(price, product_code, color, brand, material, size)
        self.sole = sole
        self.lacing = lacing

    def __str__(self):
        return "Product {} with parameters price={}, color{}, brand={}, material={}, size={}, sole={}, lacing={}".format(
            self.product_code, self.price, self.color,
            self.brand, self.material, self.size,
            self.sole, self.lacing)


class TShirts(Clothes, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', material="textile", size=44,
                 sleeves="long", collars=False):
        super(TShirts, self).__init__(price, product_code, color, brand, material, size)
        self.sleeves = sleeves
        self.collars = collars

    def __str__(self):
        return "Product {} with parameters price={}, color{}, brand={}, material={}, size={}, sleeves={}, collars={}".format(
            self.product_code, self.price, self.color,
            self.brand, self.material, self.size,
            self.sleeves, self.collars)
