from python_lab_second.task3.products import Product


class Electronics(Product):

    def __init__(self, price, product_code, color, brand, noise, screen_resolution, touch_screen, os="Android"):
        super(Electronics, self).__init__(price, product_code, color, brand)
        self.noise = noise
        self.os = os
        self.screen_resolution = screen_resolution
        self.touch_screen = touch_screen


class SmartPhones(Electronics):

    def __init__(self, price, product_code, color, brand, noise, screen_resolution, sd_card, os="Android",
                 touch_screen=True,
                 ):
        super(SmartPhones, self).__init__(price, product_code, color, brand, noise, os,
                                          screen_resolution, touch_screen)
        self.sd_card = sd_card

    def __str__(self):
        return "Product {} with parameters price={}, color={}, brand={}, noise={}db, os={}, screen_resolution={}, touch_screen={}, sd_card={}Gb".format(
            self.product_code, self.price,
            self.color, self.brand, self.noise,
            self.os, self.screen_resolution,
            self.touch_screen, self.sd_card)


class Laptops(Electronics):

    def __init__(self, price, product_code, color, brand, noise, screen_resolution, os="Android", touch_screen=True,
                 keyboard_type="forblonde"):
        super(Laptops, self).__init__(price, product_code, color, brand, noise, os,
                                      screen_resolution, touch_screen)
        self.keyboard_type = keyboard_type

    def __str__(self):
        return "Product {} with parameters price={}, color={}, brand={}, noise={}db, os={}, screen_resolution={}, touch_screen={}, keyboard_type={}".format(
            self.product_code, self.price,
            self.color, self.brand, self.noise,
            self.os, self.screen_resolution,
            self.touch_screen, self.keyboard_type)


class TV(Electronics):

    def __init__(self, price, product_code, color, brand, noise, screen_resolution, os="Android", touch_screen=True,
                 smart_tv=True,
                 binding="wall"):
        super(TV, self).__init__(price, product_code, color, brand, noise, os,
                                 screen_resolution, touch_screen)
        self.smart_tv = smart_tv
        self.binding = binding

    def __str__(self):
        return "Product {} with parameters price={}, color={}, brand={}, noise={}db, os={}, screen_resolution={}, touch_screen={}, smart_TV={}, binding={}".format(
            self.product_code, self.price,
            self.color, self.brand, self.noise,
            self.os, self.screen_resolution,
            self.touch_screen, self.smart_tv, self.binding)
