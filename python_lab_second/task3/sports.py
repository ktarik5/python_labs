from python_lab_second.task3.products import Product


class Sports(Product, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', sport_type="martial art",
                 weight=1000):
        super(Sports, self).__init__(price, product_code, color, brand)
        self.sport_type = sport_type
        self.weight = weight


class SportsFood(Sports, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', sport_type="martial art",
                 weight=1000, raw="eggwhite"):
        super(SportsFood, self).__init__(price, product_code, color, brand, sport_type, weight)
        self.raw = raw

    def __str__(self):
        return "Product {} with parameters price={}, color{}, brand={}, sport type={}, weight={}, raw={}".format(
            self.product_code, self.price, self.color,
            self.brand, self.sport_type, self.weight,
            self.raw)


class FitnessEquipment(Sports, object):

    def __init__(self, price=100, product_code=12345, color="white", brand='Xiaomi', sport_type="martial art",
                 weight=1000, shape="circle"):
        super(FitnessEquipment, self).__init__(price, product_code, color, brand, sport_type, weight)
        self.shape = shape

    def __str__(self):
        return "Product {} with parameters price={}, color{}, brand={},sport type={}, weight={}, shape={}".format(
            self.product_code, self.price, self.color,
            self.brand, self.sport_type, self.weight,
            self.shape)
