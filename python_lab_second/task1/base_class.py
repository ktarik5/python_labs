from abc import ABCMeta, abstractmethod


class Animal:
    __metaclass__ = ABCMeta

    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def get_gender(self):
        return self.gender

    @abstractmethod
    def make_sound(self):
        pass

    def eat(self, *food):
        return food

    def print_food(self, *food):
        food_string = ', '.join(food[0])
        return food_string
