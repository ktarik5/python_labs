from python_lab_second.task1.child_classes import Dog, Cat, Mouse, Frog

# task1
print("*****************TASK1***************")
dog = Dog("Brovko", 2, "M")
cat = Cat("Murka", 5, "W")
mouse = Mouse("Pyptyk", 1, "M")
frog = Frog("Prince", 10, "M")

print('ANIMALS:')

print(dog.__str__())
print(cat.__str__())
print(mouse.__str__())
print(frog.__str__())
