from python_lab_second.task1.base_class import Animal


class Dog(Animal):

    def make_sound(self):
        return "Dog says gav"

    def __str__(self):
        return "Dog {} {}, {}: {} and eats {}".format(self.get_name(), self.get_age(), self.get_gender(),
                                                      self.make_sound(), self.print_food(self.eat("meat", "pepsi")))


class Frog(Animal):

    def make_sound(self):
        return "Frog says rabit"

    def eat(self, *food):
        frog_food = [item for item in food if item.find("meat") < 0]
        return frog_food

    def __str__(self):
        return "Frog {} {}, {}: {} and eats {}".format(self.get_name(), self.get_age(), self.get_gender(),
                                                       self.make_sound(),
                                                       self.print_food(self.eat("meatbugsmeat", "supebugs")))


class Cat(Animal):

    def make_sound(self):
        return "Cat says myav"

    def __str__(self):
        return "Cat {} {}, {}: {} and eats {}".format(self.get_name(), self.get_age(), self.get_gender(),
                                                      self.make_sound(), self.print_food(self.eat("Whiskas")))


class Mouse(Animal):

    def make_sound(self):
        return "Mouse says pi"

    def __str__(self):
        return "Mouse {} {}, {}: {} and eats {}".format(self.get_name(), self.get_age(), self.get_gender(),
                                                        self.make_sound(), self.print_food(self.eat("cheese")))
